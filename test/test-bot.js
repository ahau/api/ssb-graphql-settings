const Server = require('scuttle-testbot')

const { ApolloServer } = require('apollo-server-express')
const { buildFederatedSchema } = require('@apollo/federation')
const { createTestClient } = require('apollo-server-testing')

module.exports = async function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   loadContext: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,
  //   isPataka: Boolean
  // }

  var stack = Server // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/log-stream'))

    /* @ssb-graphql/main deps */
    .use(require('ssb-blobs'))

    /* @ssb-graphql/settings deps */
    .use(require('ssb-profile'))
    .use(require('ssb-settings'))

  if (!opts.isPataka) {
    stack.use(require('ssb-box2'))
    stack = stack.use(require('ssb-tribes'))
    // required for loadContext atm
  }

  if (opts.recpsGuard || opts.loadContext) {
    stack = stack.use(require('ssb-recps-guard'))
  }

  const ssb = stack({
    ...opts,
    noDefaultUse: true,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })

  const main = require('@ssb-graphql/main')(ssb, {
    type: 'person'
  })
  const settings = require('../')(ssb)

  let context
  if (opts.loadContext) {
    context = await new Promise((resolve, reject) => {
      main.loadContext((err, context) => {
        if (err) return reject(err)
        resolve(context)
      })
    })
  }

  const apolloServer = new ApolloServer({
    schema: buildFederatedSchema([
      main,
      settings
    ]),
    context
  })

  const apollo = createTestClient(apolloServer)

  return {
    ssb,
    apollo
  }
}
