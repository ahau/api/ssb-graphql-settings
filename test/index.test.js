const test = require('tape')
const { Get, Save } = require('./lib/helpers')

const Server = require('./test-bot')

test('settings.*', async t => {
  const { apollo, ssb } = await Server()

  // init reusable helpers
  const save = Save(apollo, t)
  const get = Get(apollo, t)

  // When new user's profile is created, create their system settings
  const input = {
    keyBackedUp: false,
    recps: [ssb.id],
    authors: {
      add: ['*'] // Authors are required
    }
  }

  const id = await save(input)
  // Get the user's system settings

  const settings = await get(id)

  const expected = {
    id,
    keyBackedUp: false,
    tombstone: null,
    recps: [ssb.id]
  }
  t.deepEqual({ ...settings }, expected, 'Get correct settings')
  // When user performs certain actions, update the system settings
  const updatedInput = {
    id,
    keyBackedUp: true
  }

  const updatedId = await save(updatedInput)
  t.equal(id, updatedId, 'update returns the same Id')

  // Get the user's update settings
  const updated = await get(id)

  const expectedUpdated = {
    id,
    keyBackedUp: true,
    tombstone: null,
    recps: [ssb.id]
  }
  t.deepEqual({ ...updated }, expectedUpdated, 'Get correct settings after update')
  // Tombstone the user's settings
  const date = new Date().toISOString()

  const tombstoneInput = {
    id,
    tombstone: {
      date,
      reason: 'No longer wanted'
    }
  }

  const tombstoneId = await save(tombstoneInput)
  t.true(id, tombstoneId, 'tombstone returns the same id')

  // Get the user's settings to confirm they have been tombstoned
  const tombstoned = await get(id)

  const expectedTombstoned = {
    id,
    keyBackedUp: true,
    tombstone: {
      date: tombstoned.tombstone.date, // hack: dates werent validating properly, sometimes the test would pass, sometimes it wouldnt
      reason: 'No longer wanted'
    },
    recps: [ssb.id]
  }

  t.deepEqual({ ...tombstoned, tombstone: { ...tombstoned.tombstone } }, expectedTombstoned, 'Get correct settings after tombstone')

  // Saving two new settings records
  const id2 = await save(input)
  const id3 = await save(input)

  // Get all settings back
  const res = await apollo.query({
    query: `query {
      allSettings {
        id
        keyBackedUp
        recps
      }
    }`
  })

  t.error(res.errors, 'Gets all settings without errors')

  const expectedArray = [
    {
      id: id2,
      keyBackedUp: false,
      recps: [ssb.id]
    },
    {
      id: id3,
      keyBackedUp: false,
      recps: [ssb.id]
    }
  ]
  t.deepEqual(res.data.allSettings.map(o => ({ ...o })), expectedArray, 'Get all settings returned correct settings')

  ssb.close()
  t.end()
})
