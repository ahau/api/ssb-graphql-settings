# @ssb-graphql/settings

GraphQL types and resolvers for the ssb-settings plugin.

## Usage

Install @ssb-graphql/main and settings packages:

`npm i -S @ssb-graphql/main @ssb-graphql/artefact ssb-settings`

## Example Usage

```javascript
const { ApolloServer } = require('apollo-server-express')
const { buildFederatedSchema } = require('@apollo/federation');

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-backlinks'))
  .use(require('ssb-settings'))
  .call(null, config)

const main = require('@ssb-graphql/main')(sbot)
const settings = require('@ssb-graphql/settings')(sbot)

main.loadContext((err, context) => {
  if (err) throw err

  const server = new ApolloServer({
    schema: buildFederatedSchema([
      main,
      settings
    ]),
    context
  })
})
```
