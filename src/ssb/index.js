const { promisify: p } = require('util')

const PostSaveSettings = require('./mutations/post-save-settings')
const GetSettings = require('./queries/get-settings')
const GetAllSettings = require('./queries/get-all-settings')

module.exports = function (sbot) {
  const postSaveSettings = PostSaveSettings(sbot)
  const getSettings = GetSettings(sbot)
  const getAllSettings = GetAllSettings(sbot, getSettings)

  return {
    postSaveSettings: p(postSaveSettings),
    getSettings: p(getSettings),
    getAllSettings: p(getAllSettings),

    gettersWithCache: {
    }
  }
}
