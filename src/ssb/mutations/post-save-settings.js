const pick = require('lodash.pick')

module.exports = function PostSaveSettings (sbot) {
  return function postSaveSettings (input, cb) {
    const { id } = input
    const T = buildTransformation(input)

    if (id) {
      function cbWithId (err, _) {
        if (err) return cb(err)

        cb(null, id)
      }

      if (T.tombstone) sbot.settings.tombstone(id, T.tombstone, cbWithId)
      else sbot.settings.update(id, T, cbWithId)
      return
    }

    sbot.settings.create(T, cb)
  }
}

function buildTransformation (input) {
  const T = {}

  Object.entries(input).forEach(([key, value]) => {
    switch (key) {
      case 'id': return

      case 'tombstone':
        T[key] = pick(value, ['date', 'reason'])
        T[key].date = Number(T[key].date)
        // graphql only allows 32bit signed Ints
        // so we're passing a Date and converting it to Int for ssb
        return
      case 'authors':
        T[key] = {}
        if (value.add && value.add.length) T[key].add = value.add
        if (value.remove && value.remove.length) T[key].remove = value.remove
        return

      default:
        T[key] = value
    }
  })
  return T
}
