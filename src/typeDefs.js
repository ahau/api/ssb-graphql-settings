const gql = require('graphql-tag')

module.exports = gql`
  input SettingsAuthorSetInput {
    add: [String]
    remove: [String]
  }
  input SettingsInput {
    id: ID
    keyBackedUp: Boolean
    authors: SettingsAuthorSetInput
    tombstone: TombstoneInput
    recps: [String]
  }

  type Settings {
    id: ID
    keyBackedUp: Boolean
    tombstone: Tombstone
    recps: [String]
  }

  extend type PersonalIdentity @key(fields: "settingsId") {
    settings: Settings
  }

  extend type Query {
    settings(id: ID): Settings
    allSettings: [Settings] 
  }

  extend type Mutation {
    saveSettings(input: SettingsInput): ID
  }
`
